#include <gtest/gtest.h>

#include "../inc/Choice.hpp"
#include "../inc/Game.hpp"
#include "../inc/OptionFactory.hpp"
#include "../inc/Paper.hpp"
#include "../inc/Rock.hpp"
#include "../inc/Scissors.hpp"

namespace {
class GameTest: public testing::Test{
    public:
        Game game;
        std::string output;
};

TEST_F(GameTest, UserWins) {
    std::string expectedOutput = "You Win\n";
    
    testing::internal::CaptureStdout();
    game.compete("Rock", "Scissors");
    output = testing::internal::GetCapturedStdout();
    EXPECT_EQ(output, expectedOutput);

    testing::internal::CaptureStdout();
    game.compete("Paper", "Rock");
    output = testing::internal::GetCapturedStdout();
    EXPECT_EQ(output, expectedOutput);

    testing::internal::CaptureStdout();
    game.compete("Scissors", "Paper");
    output = testing::internal::GetCapturedStdout();
    EXPECT_EQ(output, expectedOutput);
}

TEST_F(GameTest, ComputerWins) {
    std::string expectedOutput = "Computer Wins!\n";

    testing::internal::CaptureStdout();
    game.compete("Rock", "Paper");
    output = testing::internal::GetCapturedStdout();
    EXPECT_EQ(output, expectedOutput);

    testing::internal::CaptureStdout();
    game.compete("Paper", "Scissors");
    output = testing::internal::GetCapturedStdout();
    EXPECT_EQ(output, expectedOutput);
    testing::internal::CaptureStdout();

    game.compete("Scissors", "Rock");
    output = testing::internal::GetCapturedStdout();
    EXPECT_EQ(output, expectedOutput);
}

TEST_F(GameTest, DrawGame) {
    std::string expectedOutput = "Draw Game\n";

    testing::internal::CaptureStdout();
    game.compete("Rock", "Rock");
    output = testing::internal::GetCapturedStdout();
    EXPECT_EQ(output, expectedOutput);

    testing::internal::CaptureStdout();
    game.compete("Paper", "Paper");
    output = testing::internal::GetCapturedStdout();
    EXPECT_EQ(output, expectedOutput);

    testing::internal::CaptureStdout();
    game.compete("Scissors", "Scissors");
    output = testing::internal::GetCapturedStdout();
    EXPECT_EQ(output, expectedOutput);
}
}   // namespace