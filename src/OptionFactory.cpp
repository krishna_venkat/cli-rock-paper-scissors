#include "OptionFactory.hpp"

#include <string.h>

#include <memory>

#include "Option.hpp"
#include "Paper.hpp"
#include "Rock.hpp"
#include "Scissors.hpp"
#include <iostream>

std::shared_ptr<IOption> OptionFactory::getOption(int option) {
    if (option == 1) {
        return std::shared_ptr<Rock>{new Rock};
    } else if (option == 2) {
        return std::shared_ptr<Paper>{new Paper};
    } else if (option == 3) {
        return std::shared_ptr<Scissors>{new Scissors};
    }
    return nullptr;
}
