#include "Choice.hpp"
#include "OptionFactory.hpp"
#include <iostream>

Choice::Choice(int option){
    OptionFactory            factory;
    this->Option = factory.getOption(option);
}

std::shared_ptr<IOption> Choice::getOption(){
    return this->Option;
}

Choice::~Choice(){
}