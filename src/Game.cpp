#include "Game.hpp"

#include <iostream>
#include<cstring>

void Game::compete(const char* userChoice, const char* computerChoice) {
    if (userChoice == computerChoice) {
        std::cout << "Draw Game\n";
    } else if (!strcmp(userChoice,"Rock") && !strcmp(computerChoice, "Scissors")) {
        std::cout << "You Win\n";
    } else if (!strcmp(userChoice,"Scissors") && !strcmp(computerChoice, "Paper")) {
        std::cout << "You Win\n";
    } else if (!strcmp(userChoice,"Paper") && !strcmp(computerChoice, "Rock")) {
        std::cout << "You Win\n";
    } else {
        std::cout << "Computer Wins!\n";
    }
    return;
}