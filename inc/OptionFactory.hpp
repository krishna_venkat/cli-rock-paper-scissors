#ifndef OPTION_FACTORY_HPP
#define OPTION_FACTORY_HPP

#include "Option.hpp"
#include <string.h>
#include <memory>

class IOptionFactory {
public:
	virtual std::shared_ptr<IOption> getOption(int) = 0;
};

class OptionFactory: public IOptionFactory {
public:
    std::shared_ptr<IOption> getOption(int option);
};

#endif