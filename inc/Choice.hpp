#ifndef CHOICE_HPP
#define CHOICE_HPP

#include <iostream>

#include "OptionFactory.hpp"

class IChoice {
public:
    virtual std::shared_ptr<IOption> getOption() = 0;
};

class Choice : public IChoice {
public:
    std::shared_ptr<IOption> Option;

    Choice(int);
    ~Choice();
    std::shared_ptr<IOption> getOption();
};

#endif