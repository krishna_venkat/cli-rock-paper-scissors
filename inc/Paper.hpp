#ifndef PAPER_HPP
#define PAPER_HPP

#include "Option.hpp"

class Paper : public IOption {
    public:
        const char* value = "Paper";
        const char* getValue();
};

const char* Paper::getValue(){
    return value;
}

#endif