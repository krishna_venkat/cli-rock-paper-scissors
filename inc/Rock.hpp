#ifndef ROCK_HPP
#define ROCK_HPP

#include "Option.hpp"

class Rock : public IOption {
    public:
        const char* value = "Rock";
        const char* getValue();
};

const char* Rock::getValue(){
    return value;
}

#endif