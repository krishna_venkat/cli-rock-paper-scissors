#ifndef OPTION_HPP
#define OPTION_HPP

class IOption {
public:
    virtual const char* getValue() = 0;
};

// class Choice : public IChoice {
// public:
//     const char* getValue() {
//         return "NULL";
//     }
// };

#endif