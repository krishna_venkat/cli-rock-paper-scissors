#ifndef GAME_HPP
#define GAME_HPP

class IGame {
    public:
        virtual void compete(const char*, const char*) = 0;
};

class Game: public IGame{
    public:
        void compete(const char* userChoice, const char* computerChoice);
};

#endif