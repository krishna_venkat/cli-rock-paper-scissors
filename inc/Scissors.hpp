#ifndef SCISSORS_HPP
#define SCISSORS_HPP

#include "Option.hpp"

class Scissors : public IOption {
    public:
        const char* value = "Scissors";
        const char* getValue();
};

const char* Scissors::getValue(){
    return value;
}

#endif