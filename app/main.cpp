#include <iostream>
#include <memory>
#include <string>

#include "Choice.hpp"
#include "Game.hpp"
#include "Option.hpp"

int main() {
    char replay;
    int  userEntry;
    char computerEntry = rand() % 3 + 1;

    std::shared_ptr<Choice> userChoice;
    std::shared_ptr<Choice> computerChoice;

    Game playGame;

    std::cout << "Welcome to the Legendary game of Rock, Paper and Scissors" << std::endl;
    do {
        std::cout << std::endl
                  << "What do you choose? \nRock - (1)\tPaper - (2)\tScissors - (3)\n";

        std::cin >> userEntry;
        userChoice = std::shared_ptr<Choice>{new Choice(userEntry)};

        computerEntry = rand() % 3 + 1;
        
        computerChoice = std::shared_ptr<Choice>{new Choice(computerEntry)};

        playGame.compete(userChoice->getOption()->getValue(), computerChoice->getOption()->getValue());

        std::cout << std::endl
                  << "User chose " << userChoice->getOption()->getValue() << std::endl;
        std::cout << "Computer chose " << computerChoice->getOption()->getValue() << std::endl;

        std::cout << std::endl
                  << "Would you like to play again?\n[Y]es\t[N]o\n";
        std::cin >> replay;

    } while (replay == 'y' || replay == 'Y');
}