cmake_minimum_required(VERSION 3.0)

set(This GAME)
project(${This} C CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

#set(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS}")
#set(CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS}")

set(CMAKE_LIBRARY_OUTPUT_DIRECTORY lib)

add_subdirectory(test)

include_directories(
    inc
)

# Some recommendations on NOT using file globbing to grab all cpp files.  So far, it seems
# ok, so rolling with it until we get more experience.

file(GLOB GAME_LIB_SRC
    "src/*.cpp"
)

file(GLOB GAME_APP_SRC
    "app/*.cpp"
)

add_library(${This} ${GAME_LIB_SRC})

include(DownloadProject.cmake)
download_project(PROJ                googletest
                 GIT_REPOSITORY      https://github.com/google/googletest.git
                 GIT_TAG             master
                 PREFIX              ${CMAKE_SOURCE_DIR}/ext_modules           
)

add_executable(game ${GAME_APP_SRC} ${GAME_LIB_SRC})

add_subdirectory(${googletest_SOURCE_DIR} ${googletest_BINARY_DIR})

